#ifndef FENETRE_SIMULATION_H
#define FENETRE_SIMULATION_H

#pragma once

#include <QGraphicsScene>     // va contenir tous les éléments (=objets) de l’interface
#include <QGraphicsRectItem>
#include <QGraphicsEllipseItem>
#include <QGraphicsSceneMouseEvent>
#include <QPen>
#include <QBrush>
#include <QLine>
#include <QColor>
#include <QTimer>

#include "fenetre_newsimulation.h"
#include "../headers/client.h"
#include "../headers/stats.h"

#define LONGUEUR 1200
#define LARGEUR 600

class Fenetre_simulation : public  QGraphicsScene
{
Q_OBJECT

public slots:
    void creer_fenetre_menu();
    void push_quitter_application();
    void push_lecPause();
    void push_sauvegarde();
    void push_valider();

    void simulation();

    void active_desactive();

public:
    Fenetre_simulation(const string path);
    Fenetre_simulation(const int nb_client, const float setup[][2]);

protected:
    void mouseMoveEvent(QGraphicsSceneMouseEvent *mouseEvent);

private:
    QPushButton* quitter; //Déclenche a fermeture de la fenêtre de simulation
    QPushButton* retourMenu; //Ferme la fenetre de simulation et ouvre le menu
    QPushButton* sauvegarde; // Permet de sauvegarder l'état actuel de la simulation
    QPushButton* lecPause; //Suspend a simulation ou la relance
    QPushButton* valider; //Valider les changements des taux d'émmissions de cellules des clients

    QCheckBox* active[10]; //Active ou désactive les clients
    QLine*  mb_format[9]; //Des éléments graphiques permettant de clarifier la séparation entre les differents clients
    QLabel* mb_txPerteGlob; // Label permettant d'afficher le taux de perte global
    QLabel* mb_nbCellEmises;//Label permettant d'afficher le nombre de cellules emises
    QLabel* mb_nbCellPerdues;
    QLineEdit*  mb_EmissionActClient[10]; //peut etre modifier pendant la simulation
    QDoubleValidator *limite;
    QLabel* mb_GenJetonclient[10]; //valeur stockée dans le client
    QLabel* mb_txPerteClient[10];
    // sera utilisé pour afficher des informations diverses comme la proposition de changer le taux d’émission d’un client
    QLabel* mb_texte;

    QTimer* timer;
    QGraphicsEllipseItem* mb_tokenGraph[10][5]; // un tableau représentant la composante graphique des jetons pour chaque client
                                                // on affiche 5 jetons par Client
    QGraphicsRectItem* mb_cellGraph[10][5];     // un tableau représentant la composante graphique des cellules pour chaque client
                                                // on affiche 5 cellules par Client
    int tabMovCell[10];                      // le tableau qui va servir à comptabiliser le nombre mouvement que vont devoir effectuer les cellules et les jetons graphiquement

    Client client[10];
    int nb_client; //contient le nombre de clients de la simulation.
    int clientSelect;
    int a; //incrémente les mouvements
};

#endif // FENETRE_SIMULATION_H
