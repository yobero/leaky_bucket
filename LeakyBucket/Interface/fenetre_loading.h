#ifndef FENETRE_LOADING_H
#define FENETRE_LOADING_H

#pragma once

#include "fenetre_menu.h"
#include "../headers/save.h"

#include <dirent.h>
#include <QListWidget>
#include <stdio.h>

class Fenetre_loading : public QWidget
{
Q_OBJECT

public:
    Fenetre_loading();

public slots:
    void creer_fenetre_simulation();
    void creer_fenetre_menu();

private:
  QLabel *m_texte;
  QListWidget *m_listSauv;
  QPushButton *m_bouton_valider;
  QPushButton *m_bouton_retourMenu;
};

#endif // FENETRE_LOADING_H
