#include "fenetre_simulation.h"

Fenetre_simulation::Fenetre_simulation(const string path){
    this->nb_client = nb_client;
    clientSelect=-1;

    setSceneRect(0,0,LONGUEUR,LARGEUR);

    limite=new QDoubleValidator(0.0,10.9,1,this);
    limite->setLocale(QLocale::C);//permet de mettre un '.' au lieu de ','

    //Bouton
        quitter = new QPushButton("Quitter");
        quitter->setToolTip("Quitter le programme");
        quitter->setGeometry(0,0,150,20);
        addWidget(quitter);

        retourMenu = new QPushButton("Retour au menu");
        retourMenu->setGeometry(150,0,150,20);
        addWidget(retourMenu);

        sauvegarde = new QPushButton("Sauvegarde");
        sauvegarde->setGeometry(300,0,150,20);
        sauvegarde->setToolTip("Sauvegarder l'etat actuel de la simulation");
        addWidget(sauvegarde);

        lecPause = new QPushButton("Lecture");
        lecPause->setGeometry(LONGUEUR-300,LARGEUR-50,150,50);
        addWidget(lecPause);

        valider = new QPushButton("Valider");
        valider->setGeometry(LONGUEUR-150,LARGEUR-50,150,50);
        valider->setToolTip("Permet de modifier le taux d'émission actuel des clients");
        addWidget(valider);

    for(int i = 0; i<10;i++){
      client[i]= Client(i,0,1);
    }
    nb_client = lecture(path,client); // création des clients en fonction d'une sauvegarde

    //checkBox
        for(int i=0;i<nb_client;i++){
            active[i] = new QCheckBox("Désactiver");
            mb_EmissionActClient[i] = new QLineEdit(QString::number((float) client[i].get_transmit_rate()));

            mb_GenJetonclient[i] = new QLabel("Jetons : " + QString::number((float)client[i].get_tokens_rate()));
            mb_txPerteClient[i] = new QLabel("Taux de perte : " + QString::number((float) lost_cells_rate(client[i])) + " %");

            active[i]->setChecked(true);
            if(i<5){
                active[i]->setGeometry((LONGUEUR/6)*(i+1)+10,3*(LARGEUR/7),100,20);
                mb_EmissionActClient[i]->setGeometry((LONGUEUR/6)*(i+1)+110,3*(LARGEUR/7),75,20);

                mb_GenJetonclient[i]->setGeometry((LONGUEUR/6)*(i+1)+10,3*(LARGEUR/7)-28,180,25);
                mb_txPerteClient[i]->setGeometry((LONGUEUR/6)*(i+1)+10,3*(LARGEUR/7)-60,180,25);
            }
            else{
                active[i]->setGeometry((LONGUEUR/6)*(i-4)+10,11*(LARGEUR/13),100,20);
                mb_EmissionActClient[i]->setGeometry((LONGUEUR/6)*(i-4)+110,11*(LARGEUR/13),75,20);

                mb_GenJetonclient[i]->setGeometry((LONGUEUR/6)*(i-4)+10,11*(LARGEUR/13)-28,180,25);
                mb_txPerteClient[i]->setGeometry((LONGUEUR/6)*(i-4)+10,11*(LARGEUR/13)-60,180,25);
            }
            addWidget(active[i]);
            addWidget(mb_GenJetonclient[i]);
            addWidget(mb_txPerteClient[i]);

            connect(active[i],SIGNAL(stateChanged(int)),this,SLOT(active_desactive()));

            mb_EmissionActClient[i]->setValidator(limite);
            addWidget(mb_EmissionActClient[i]);
        }

        mb_format[0]=new QLine(LONGUEUR/6,32,LONGUEUR/6,LARGEUR);
        mb_format[1]=new QLine(2*LONGUEUR/6,32,2*LONGUEUR/6,9*LARGEUR/10);
        mb_format[2]=new QLine(3*LONGUEUR/6,32,3*LONGUEUR/6,9*LARGEUR/10);
        mb_format[3]=new QLine(4*LONGUEUR/6,32,4*LONGUEUR/6,9*LARGEUR/10);
        mb_format[4]=new QLine(5*LONGUEUR/6,32,5*LONGUEUR/6,9*LARGEUR/10);
        mb_format[5]=new QLine(LONGUEUR,32,LONGUEUR,9*LARGEUR/10);
        mb_format[6]=new QLine(LONGUEUR/6,(32+9*LARGEUR/10)/2,LONGUEUR,(32+9*LARGEUR/10)/2);
        mb_format[7]=new QLine(LONGUEUR/6,32,LONGUEUR,32);
        mb_format[8]=new QLine(LONGUEUR/6,9*LARGEUR/10,LONGUEUR,9*LARGEUR/10);

        QPen pen_format(Qt::darkGray,10,Qt::SolidLine);

        for (int i=0;i<9;i++){
          this->addLine(*mb_format[i],pen_format);
        }

        mb_texte = new QLabel("Ici sera affiché des informations pour l'utilisateur");
        mb_texte->setGeometry(LONGUEUR/4,10*LARGEUR/11,LONGUEUR/3,LARGEUR/12);
        addWidget(mb_texte);

        mb_txPerteGlob= new QLabel("Taux de perte global: \n"+ QString::number((float) total_lost_cells_rate(client,nb_client)) + " %");
        mb_txPerteGlob->setGeometry(0,LARGEUR/4,LONGUEUR/8,LARGEUR/6);
        addWidget(mb_txPerteGlob);

        mb_nbCellEmises= new QLabel("Nombre de Cellules émises: \n"+QString::number((float) total_sent_cells(client,nb_client)));
        mb_nbCellEmises->setGeometry(0,LARGEUR/2,2*LONGUEUR/13,LARGEUR/6);
        addWidget(mb_nbCellEmises);

        mb_nbCellPerdues= new QLabel("Nombre de Cellules perdue: \n" + QString::number((float) total_lost_cells(client,nb_client)));
        mb_nbCellPerdues->setGeometry(0,3*LARGEUR/4,2*LONGUEUR/13,LARGEUR/6);
        addWidget(mb_nbCellPerdues);

    connect(quitter,SIGNAL(clicked(bool)),this,SLOT(push_quitter_application()));
    connect(retourMenu,SIGNAL(clicked(bool)),this,SLOT(creer_fenetre_menu()));
    connect(lecPause,SIGNAL(clicked(bool)),this,SLOT(push_lecPause()));
    connect(sauvegarde,SIGNAL(clicked(bool)),this,SLOT(push_sauvegarde()));
    connect(valider,SIGNAL(clicked(bool)),this,SLOT(push_valider()));

    timer = new QTimer();
    this->a = -1;
    timer->setInterval(75); // 1 s == 1000 ms
    connect(timer,SIGNAL(timeout()),this,SLOT(simulation()));

    QPen pen(Qt::black,1,Qt::SolidLine);
    QBrush brush_cell(Qt::darkCyan);
    QBrush brush_token(Qt::green);

    //Déclaration des attributs graphiques (Cellules puis jetons)
    for(int i=0;i<nb_client;i++){
      tabMovCell[i]=0;
      //Déclaration des cellules graphiques et des jetons
      for (int y=0;y<5;y++){
        if(i<5)
            this->mb_cellGraph[i][y]=new QGraphicsRectItem((LONGUEUR/6)*(i+1)-y*30+LONGUEUR/10+10,3*(LARGEUR/13)-80,25,15);
        else
            this->mb_cellGraph[i][y]=new QGraphicsRectItem((LONGUEUR/6)*(i-4)-y*30+LONGUEUR/10+10,8*(LARGEUR/13)-50,25,15);

        this->mb_cellGraph[i][y]->setPen(pen);this->mb_cellGraph[i][y]->setBrush(brush_cell);
        this->mb_cellGraph[i][y]->setVisible(false);
        this->addItem(this->mb_cellGraph[i][y]);

        if(i<5)
            this->mb_tokenGraph[i][y]=new QGraphicsEllipseItem((LONGUEUR/6)*(i+2)-64,3*(LARGEUR/13)-60+y*20,14,14);
        else
            this->mb_tokenGraph[i][y]=new QGraphicsEllipseItem((LONGUEUR/6)*(i-3)-64,8*(LARGEUR/13)-30+y*20,14,14);

        this->mb_tokenGraph[i][y]->setPen(pen);this->mb_tokenGraph[i][y]->setBrush(brush_token);
        this->mb_tokenGraph[i][y]->setVisible(true);
        this->addItem(this->mb_tokenGraph[i][y]);

      }

    }
    //TEST (affiche une valeur entrer dans fenetre_newSimulation)
  /*  QLabel *t = new QLabel(QString::number((float)setup[0][0])+ " " + QString::number((float) setup[0][1]) + " " + QString::number((float) setup[1][0]));
    t->setGeometry(500,200,100,50);
    addWidget(t);*/
}

Fenetre_simulation::Fenetre_simulation(const int nb_client, const float setup[][2]) : QGraphicsScene()
{
    this->nb_client = nb_client;
    clientSelect=-1;

    setSceneRect(0,0,LONGUEUR,LARGEUR);

    limite=new QDoubleValidator(0.0,10.9,1,this);
    limite->setLocale(QLocale::C);//permet de mettre un '.' au lieu de ','

    //Bouton
        quitter = new QPushButton("Quitter");
        quitter->setToolTip("Quitter le programme");
        quitter->setGeometry(0,0,150,20);
        addWidget(quitter);

        retourMenu = new QPushButton("Retour au menu");
        retourMenu->setGeometry(150,0,150,20);
        addWidget(retourMenu);

        sauvegarde = new QPushButton("Sauvegarde");
        sauvegarde->setGeometry(300,0,150,20);
        sauvegarde->setToolTip("Sauvegarder l'etat actuel de la simulation");
        addWidget(sauvegarde);

        lecPause = new QPushButton("Lecture");
        lecPause->setGeometry(LONGUEUR-300,LARGEUR-50,150,50);
        addWidget(lecPause);

        valider = new QPushButton("Valider");
        valider->setGeometry(LONGUEUR-150,LARGEUR-50,150,50);
        valider->setToolTip("Permet de modifier le taux d'émission actuel des clients");
        addWidget(valider);

    //checkBox
        for(int i=0;i<nb_client;i++){
            active[i] = new QCheckBox("Désactiver");
            mb_EmissionActClient[i] = new QLineEdit(QString::number((float) setup[i][0]));

            mb_GenJetonclient[i] = new QLabel("Jetons : " + QString::number((float)setup[i][1]));
            mb_txPerteClient[i] = new QLabel("Taux de perte : 0 %");

            active[i]->setChecked(true);
            if(i<5){
                active[i]->setGeometry((LONGUEUR/6)*(i+1)+10,3*(LARGEUR/7),100,20);
                mb_EmissionActClient[i]->setGeometry((LONGUEUR/6)*(i+1)+110,3*(LARGEUR/7),75,20);

                mb_GenJetonclient[i]->setGeometry((LONGUEUR/6)*(i+1)+10,3*(LARGEUR/7)-28,180,25);
                mb_txPerteClient[i]->setGeometry((LONGUEUR/6)*(i+1)+10,3*(LARGEUR/7)-60,180,25);
            }
            else{
                active[i]->setGeometry((LONGUEUR/6)*(i-4)+10,11*(LARGEUR/13),100,20);
                mb_EmissionActClient[i]->setGeometry((LONGUEUR/6)*(i-4)+110,11*(LARGEUR/13),75,20);

                mb_GenJetonclient[i]->setGeometry((LONGUEUR/6)*(i-4)+10,11*(LARGEUR/13)-28,180,25);
                mb_txPerteClient[i]->setGeometry((LONGUEUR/6)*(i-4)+10,11*(LARGEUR/13)-60,180,25);
            }
            addWidget(active[i]);
            addWidget(mb_GenJetonclient[i]);
            addWidget(mb_txPerteClient[i]);

            connect(active[i],SIGNAL(stateChanged(int)),this,SLOT(active_desactive()));

            //Coordonnées à modifier
            mb_EmissionActClient[i]->setValidator(limite);
            mb_EmissionActClient[i]->setText(QString::number((float) setup[i][0])); //modifier setup par le client à la fin
            addWidget(mb_EmissionActClient[i]);


        }

        mb_format[0]=new QLine(LONGUEUR/6,32,LONGUEUR/6,LARGEUR);
        mb_format[1]=new QLine(2*LONGUEUR/6,32,2*LONGUEUR/6,9*LARGEUR/10);
        mb_format[2]=new QLine(3*LONGUEUR/6,32,3*LONGUEUR/6,9*LARGEUR/10);
        mb_format[3]=new QLine(4*LONGUEUR/6,32,4*LONGUEUR/6,9*LARGEUR/10);
        mb_format[4]=new QLine(5*LONGUEUR/6,32,5*LONGUEUR/6,9*LARGEUR/10);
        mb_format[5]=new QLine(LONGUEUR,32,LONGUEUR,9*LARGEUR/10);
        mb_format[6]=new QLine(LONGUEUR/6,(32+9*LARGEUR/10)/2,LONGUEUR,(32+9*LARGEUR/10)/2);
        mb_format[7]=new QLine(LONGUEUR/6,32,LONGUEUR,32);
        mb_format[8]=new QLine(LONGUEUR/6,9*LARGEUR/10,LONGUEUR,9*LARGEUR/10);

        QPen pen_format(Qt::darkGray,10,Qt::SolidLine);

        for (int i=0;i<9;i++){
          this->addLine(*mb_format[i],pen_format);
        }

        mb_texte = new QLabel("Ici sera affiché des informations pour l'utilisateur");
        mb_texte->setGeometry(LONGUEUR/4,10*LARGEUR/11,LONGUEUR/3,LARGEUR/12);
        addWidget(mb_texte);

        mb_txPerteGlob= new QLabel("Taux de perte global: \n"+ QString::number(0) + " %");
        mb_txPerteGlob->setGeometry(0,LARGEUR/4,LONGUEUR/8,LARGEUR/6);
        addWidget(mb_txPerteGlob);

        mb_nbCellEmises= new QLabel("Nombre de Cellules émises: \n"+QString::number(0));
        mb_nbCellEmises->setGeometry(0,LARGEUR/2,2*LONGUEUR/13,LARGEUR/6);
        addWidget(mb_nbCellEmises);

        mb_nbCellPerdues= new QLabel("Nombre de Cellules perdues: \n" + QString::number(0));
        mb_nbCellPerdues->setGeometry(0,3*LARGEUR/4,2*LONGUEUR/13,LARGEUR/6);
        addWidget(mb_nbCellPerdues);

    connect(quitter,SIGNAL(clicked(bool)),this,SLOT(push_quitter_application()));
    connect(retourMenu,SIGNAL(clicked(bool)),this,SLOT(creer_fenetre_menu()));
    connect(lecPause,SIGNAL(clicked(bool)),this,SLOT(push_lecPause()));
    connect(sauvegarde,SIGNAL(clicked(bool)),this,SLOT(push_sauvegarde()));
    connect(valider,SIGNAL(clicked(bool)),this,SLOT(push_valider()));

    timer = new QTimer();
    this->a = -1;
    timer->setInterval(75); // 1 s == 1000 ms
    connect(timer,SIGNAL(timeout()),this,SLOT(simulation()));

    //Déclaration des Clients
    for(int i=0;i<nb_client;i++){
        client[i] = Client(i,setup[i][0],setup[i][1]);
    }
    QPen pen(Qt::black,1,Qt::SolidLine);
    QBrush brush_cell(Qt::darkCyan);
    QBrush brush_token(Qt::green);

    //Déclaration des attributs graphiques (Cellules puis jetons)
    for(int i=0;i<nb_client;i++){
      tabMovCell[i]=0;
      //Déclaration des cellules graphiques et des jetons
      for (int y=0;y<5;y++){
        if(i<5)
            this->mb_cellGraph[i][y]=new QGraphicsRectItem((LONGUEUR/6)*(i+1)-y*30+LONGUEUR/10+10,3*(LARGEUR/13)-80,25,15);
        else
            this->mb_cellGraph[i][y]=new QGraphicsRectItem((LONGUEUR/6)*(i-4)-y*30+LONGUEUR/10+10,8*(LARGEUR/13)-50,25,15);

        this->mb_cellGraph[i][y]->setPen(pen);this->mb_cellGraph[i][y]->setBrush(brush_cell);
        this->mb_cellGraph[i][y]->setVisible(false);
        this->addItem(this->mb_cellGraph[i][y]);

        if(i<5)
            this->mb_tokenGraph[i][y]=new QGraphicsEllipseItem((LONGUEUR/6)*(i+2)-64,3*(LARGEUR/13)-60+y*20,14,14);
        else
            this->mb_tokenGraph[i][y]=new QGraphicsEllipseItem((LONGUEUR/6)*(i-3)-64,8*(LARGEUR/13)-30+y*20,14,14);

        this->mb_tokenGraph[i][y]->setPen(pen);this->mb_tokenGraph[i][y]->setBrush(brush_token);
        this->mb_tokenGraph[i][y]->setVisible(true);
        this->addItem(this->mb_tokenGraph[i][y]);

      }

    }
    //TEST (affiche une valeur entrer dans fenetre_newSimulation)
  /*  QLabel *t = new QLabel(QString::number((float)setup[0][0])+ " " + QString::number((float) setup[0][1]) + " " + QString::number((float) setup[1][0]));
    t->setGeometry(500,200,100,50);
    addWidget(t);*/
}

void Fenetre_simulation::mouseMoveEvent(QGraphicsSceneMouseEvent *mouseEvent){
    for(int i=0;i<nb_client;i++){
        if(i<5){
            if((mouseEvent->scenePos().x() >= (LONGUEUR/6)*(i+1) && mouseEvent->scenePos().x() <= ((LONGUEUR/6)*(i+1))+200) && (mouseEvent->scenePos().y() <= 3*(LARGEUR/7)+20 && mouseEvent->scenePos().y() >= LARGEUR/15)){
                clientSelect=i;
                mb_texte->setText("Client "+ QString::number(i) + "    Cellules émises : " + QString::number(client[i].get_nb_cell_transmit()) +
                                  "\n                   Cellules perdues : " + QString::number(client[i].get_nb_lost_cell()) +
                                  "\n                   Taux de perte : " + QString::number((float) lost_cells_rate(client[i])) + " %");
                break;
            }
        }
        else{
            if((mouseEvent->scenePos().x() >= (LONGUEUR/6)*(i-4) && mouseEvent->scenePos().x() <= ((LONGUEUR/6)*(i-4))+200) && mouseEvent->scenePos().y() <= 11*(LARGEUR/13)+20 && mouseEvent->scenePos().y() >= (LARGEUR/2)-5){
                clientSelect=i;
                mb_texte->setText("Client "+ QString::number(i) + "    Cellules émises : " + QString::number(client[i].get_nb_cell_transmit()) +
                                  "\n                   Cellules perdues : " + QString::number(client[i].get_nb_lost_cell()) +
                                  "\n                   Taux de perte : " + QString::number((float) lost_cells_rate(client[i])) + " %");
                break;
            }
        }
    }
}

void Fenetre_simulation::push_quitter_application(){
    this->setSceneRect(1,1,1,1);
}

void Fenetre_simulation::creer_fenetre_menu(){
    this->setSceneRect(1,1,1,1);//Par défaut le rectangle de la scène est (0,0,0,0)
    Fenetre_menu* menu = new Fenetre_menu();
    menu->show();
}

void Fenetre_simulation::push_lecPause(){
    if(lecPause->text() == "Lecture"){//Simulation en pause
        lecPause->setText("Pause");
        timer->start();
    }
    else{//Simulation en cours
        lecPause->setText("Lecture");
        timer->stop();
    }
}

void Fenetre_simulation::push_sauvegarde(){
    mb_texte->setText("Sauvegarde effectuée");
    ecriture(client,nb_client);
}

void Fenetre_simulation::push_valider(){
    mb_texte->setText("Vos changements ont été effectués");
    for(int i=0; i<nb_client;i++){
        client[i].set_transmit_rate(mb_EmissionActClient[i]->text().toFloat());
    }
    mb_texte->setText(QString::number((float)client[0].get_transmit_rate()));
}

void Fenetre_simulation::simulation(){
    //TEST "boucle infinie"
    //static int a =-1;
    a++;
    //mb_texte->setText(" A = " + QString::number(a));
    if(a%25==0){
      for(int i=0;i<nb_client;i++){
        if(clientSelect == i){
            mb_texte->setText("Cellules émises : " + QString::number(client[i].get_nb_cell_transmit()) +
                              "\nCellules perdues : " + QString::number(client[i].get_nb_lost_cell()) +
                              "\nTaux de perte : " + QString::number((float) lost_cells_rate(client[i])) + " %");
        }
        tabMovCell[i]=0;
          if(active[i]->isChecked()) {                                          //Test si le client est activé
              for(int j=0;j<client[i].get_tokens_rate();j++){
                  client[i].get_tokens().add_Token();
              }

              for(int j=0;j<client[i].get_transmit_rate();j++){
                  if(client[i].get_cells().add_Cell(Cell(i+1,(i%3)+1)) == true){
                      client[i].set_nb_lost_cell(client[i].get_nb_lost_cell()+1);
                  }
              }
              client[i].set_nb_cell_transmit(client[i].get_nb_cell_transmit() + client[i].get_transmit_rate());
              //on remet les données graphiques à leur position d'origine et on affiche seulement les cellules/token qui sont stockés
              for(int j=0;j<5;j++){

                if(client[i].get_cells().get_cells()[j].getPriority()!=-1){
                  mb_cellGraph[i][j]->setVisible(true);
                  if(i<5)
                      this->mb_cellGraph[i][j]->setPos(0,0);
                  else
                      this->mb_cellGraph[i][j]->setPos(0,0);
                }
                else
                  mb_cellGraph[i][j]->setVisible(false);

                if(client[i].get_tokens().get_bufferToken()[j]){
                  mb_tokenGraph[i][j]->setVisible(true);
                  if(i<5)
                      this->mb_tokenGraph[i][j]->setPos(0,0);
                  else
                      this->mb_tokenGraph[i][j]->setPos(0,0);
                }
                else
                  mb_tokenGraph[i][j]->setVisible(false);
              }

              while(client[i].get_tokens().get_bufferToken()[0] != 0 && client[i].get_cells().cells_void() == false){
                  client[i].get_tokens().move_to_left();
                  client[i].get_cells().move_to_left();
                  tabMovCell[i]+=5;// on incrémente le nombre de mouvement que vont effectuer les cellules
              }

              mb_txPerteClient[i]->setText("Taux de perte : " + QString::number((float) lost_cells_rate(client[i])) + " %");

          }

      }
      mb_txPerteGlob->setText("Taux de perte globale :\n" + QString::number((float) total_lost_cells_rate(client,nb_client)) + " %");
      mb_nbCellEmises->setText("Nombre de Cellules émises: \n"+ QString::number(total_sent_cells(client, nb_client)));
      mb_nbCellPerdues->setText("Nombre Cellules perdues: \n"+ QString::number(total_lost_cells(client, nb_client)));
    }

    for(int i=0;i<nb_client;i++){
      // on a marqué le mouvement précedemment pour pouvoir effacer la denrière cellule/token
      if(tabMovCell[i]==-1){
        mb_cellGraph[i][(a%25/5)-1]->setVisible(false);
        mb_tokenGraph[i][(a%25/5)-1]->setVisible(false);
        tabMovCell[i]=0;
      }
      if(tabMovCell[i]>0){

        if(a%5==0){
          mb_tokenGraph[i][(a%25)/5]->setPos(0,mb_tokenGraph[i][(a%25)/5]->y()-19);

          if((a%25)>0){
            mb_cellGraph[i][(a%25/5)-1]->setVisible(false);
            mb_tokenGraph[i][(a%25/5)-1]->setVisible(false);
          }
        }

        for(int j=0;j<(4-(a%25)/5);j++){
          mb_tokenGraph[i][4-j]->setPos(0,mb_tokenGraph[i][4-j]->y()-4);
        }

        mb_tokenGraph[i][(a%25)/5]->setPos(mb_tokenGraph[i][(a%25)/5]->x()+6,mb_tokenGraph[i][(a%25)/5]->y());

        for(int j=0;j<5;j++){
          mb_cellGraph[i][j]->setPos(mb_cellGraph[i][j]->x()+6,0);
        }
        // si c'est le dernier mouvement de ce cycle on le marque pour pouvoir effacer la cellule qui est envoyées dans le réseau
        if(tabMovCell[i]==1)
          tabMovCell[i]--;
        tabMovCell[i]--;
      }

    }
    timer->start();
}

//Peut_etre inutile
void Fenetre_simulation::active_desactive(){
    for(int i=0;i<nb_client;i++){
        if(active[i]->isChecked())
            active[i]->setText("Désactiver");
        else
            active[i]->setText("Activer");
    }
}
