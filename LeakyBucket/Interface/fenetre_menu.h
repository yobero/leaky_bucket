#ifndef FENETRE_MENU_H
#define FENETRE_MENU_H

#pragma once

#include <QPushButton>        // Contient la création et la gestion des boutons
#include <QGraphicsScene>     // Va contenir tous les éléments (=objets) de l’interface
#include <QLabel> 	      // Permet d’afficher aussi du texte (les données à afficher)
#include <QCheckBox>          // Utilisable pour activer_désactiver des clients
#include <QSpinBox>           // Permet de créer des “boîtes” où l’utilisateur peut entrer des données
#include <QLineEdit>
#include <QString>
#include <QFont>

#include "fenetre_newsimulation.h"
#include "fenetre_loading.h"

/*
* La fenetre menu est une fenetre permettant à l'utilisateur de faire
* un choix entre commencer une nouvelle simulation, ou bien en charger
* une précédemment sauvegardée
*/
class Fenetre_menu : public QWidget
{
Q_OBJECT

public:
    Fenetre_menu();

public slots:
    void push_new_simulation();	// Creer la fenetre permettant d'initialiser une nouvelle simulation
    void push_loading(); // Creer la fenetre permettant de charger une ancienne simulation

private :
    QPushButton *m_bouton_newSimulation; //bouton servant à sélectionner le lancement d’une nouvelle simulation
    QPushButton *m_bouton_loading;       // bouton servant à sélectionner le chargement d’une sauvegarde
    QSpinBox *m_nb_clients;              // menu déroulant (de 1 à 10 ) pour choisir le nombre de clients lors de la création d’une nouvelle simulation
    QLabel *m_lb_Titre;      // affiche du titre du simulateur
    QLabel *m_lb_texte;     // affiche le texte d’introduction “souhaitez vous commencer une nouvelle simulation ou en charger une ?

};

#endif // FENETRE_MENU_H
