#include "fenetre_loading.h"

Fenetre_loading::Fenetre_loading() : QWidget()
{
  //Taille de la fenetre
  setFixedSize(800,600);

  DIR* rep = NULL;
  struct dirent* fichierLu=NULL;
  m_listSauv=new QListWidget(this);
  rep = opendir("./Sauvegardes"); // Ouverture d'un dossier
  //if (rep == NULL); //Si le dossier n'a pas pu être ouvert
  while ((fichierLu = readdir(rep)) != NULL){
    if(fichierLu->d_name[0]!='.')
      m_listSauv->addItem(QString(fichierLu->d_name));
  }
  if (closedir(rep) == -1) // S'il y a eu un souci avec la fermeture
    exit(-1);

  m_listSauv->setGeometry(225,100,350,300);// Affichage de la liste des sauvegardes disponibles

  m_texte=new QLabel("   Selectionnez une \n       Sauvegarde",this);
  m_texte->setGeometry(250,0,300,100);
  m_texte->setFont(QFont("Comic Sans MS", 20));

  m_bouton_retourMenu = new  QPushButton("Retourner Au Menu",this);
  m_bouton_retourMenu->setGeometry(50,530,350,50);

  m_bouton_valider = new QPushButton("Valider",this);
  m_bouton_valider->setGeometry(400,530,350,50);

  connect(m_bouton_valider,SIGNAL(clicked()),this,SLOT(creer_fenetre_simulation()));
  connect(m_bouton_retourMenu,SIGNAL(clicked()),this,SLOT(creer_fenetre_menu()));

}

void Fenetre_loading::creer_fenetre_simulation(){
  // reprise de la sauvegarde ici
  if(!m_listSauv->selectedItems().isEmpty() && m_listSauv->selectedItems().size() == 1){
      m_texte->setText(m_listSauv->selectedItems().takeFirst()->text());
      string path = "Sauvegardes/" + m_listSauv->selectedItems().takeFirst()->text().toStdString();

      this->close();
      Fenetre_simulation* simulation = new Fenetre_simulation(path);
      QGraphicsView* vue = new QGraphicsView(simulation);
      vue->setMouseTracking(true);
      vue->setFixedSize(LONGUEUR+5,LARGEUR+5);
      vue->setSceneRect(vue->sceneRect());
      vue->show();
      connect(simulation,SIGNAL(sceneRectChanged(QRectF)),vue,SLOT(close()));
  }
}
void Fenetre_loading::creer_fenetre_menu(){
  this->close();
  Fenetre_menu* menu = new Fenetre_menu();
  menu->show();
}
