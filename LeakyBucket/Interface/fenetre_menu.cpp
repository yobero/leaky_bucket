#include "fenetre_menu.h"

Fenetre_menu::Fenetre_menu() : QWidget()
{
    //Taille de la fenetre
    setFixedSize(800,600);

    m_lb_Titre = new QLabel("  Simulateur \n        de  \nLeaky Bucket",this);
    m_lb_Titre->setGeometry(300,0,300,110);  // (x,y,lenth,width)
    m_lb_Titre->setFont(QFont("Comic Sans MS", 20));

    m_lb_texte = new QLabel("Selectionnez le nombre de clients : ",this);
    m_lb_texte->setGeometry(225,150,275,50);
    m_lb_texte->setFont(QFont("Comic Sans MS", 11));

    m_nb_clients = new QSpinBox(this);
    m_nb_clients->setRange(1,10);
    m_nb_clients->setGeometry(500,160,50,30);

    m_bouton_newSimulation = new  QPushButton("Lancer une nouvelle simulation",this);
    m_bouton_newSimulation->setGeometry(225,225,350,50);
    m_bouton_newSimulation->setToolTip("Permet de lancer une nouvelle simulation");


    m_bouton_loading = new QPushButton("Charger une simulation",this);
    m_bouton_loading->setGeometry(225,330,350,50);

    connect(m_bouton_newSimulation,SIGNAL(clicked()),this,SLOT(push_new_simulation()));
    connect(m_bouton_loading,SIGNAL(clicked()),this,SLOT(push_loading()));
}

void Fenetre_menu::push_new_simulation(){
    this->close();
    Fenetre_newSimulation* newSimulation = new Fenetre_newSimulation(m_nb_clients->value());
    newSimulation->show();
}

void Fenetre_menu::push_loading(){
  this->close();
  Fenetre_loading* loading = new Fenetre_loading();
  loading->show();
}
