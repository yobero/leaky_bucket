#include "fenetre_newsimulation.h"

Fenetre_newSimulation::Fenetre_newSimulation(const int nb_client) : QWidget()
{
    setFixedSize(800,600);

    this->nb_client=nb_client;

    m_texte=new QLabel("Création des clients :",this);
    m_texte->setGeometry(270,0,300,50);
    m_texte->setFont(QFont("Comic Sans MS", 20));

    QString str="Client ";

    limite=new QDoubleValidator(0,10,3,this); //Le maximum ne marche pas (on peut mettre 11)
    limite->setLocale(QLocale::C);

    m_config_client[0] = new QLabel("Taux d'émissions actuel",this);
    m_config_client[0]->setGeometry(200,50,200,50);

    m_config_client[1] = new QLabel("Taux de génération des jetons",this);
    m_config_client[1]->setGeometry(450,50,250,50);

    for(int i=0;i<nb_client;i++){
      m_numClient[i]=new QLabel(str + QString::number(i+1) + " :",this);
      m_numClient[i]->setGeometry(50,100+i*(400/nb_client),60,50);  // (x,y,lenth,width)
      m_numClient[i]->setFont(QFont("Comic Sans MS", 10));

      //Taux d'émssion du client
      m_le_setUp[i][0]=new QLineEdit(this);
      m_le_setUp[i][0]->setGeometry(225,100+nb_client+i*(400/nb_client),100,50-nb_client*1.5);
      m_le_setUp[i][0]->setValidator(limite);

      //Taux de géneration des jetons
      m_le_setUp[i][1] = new QLineEdit(this);
      m_le_setUp[i][1]->setGeometry(500,100+nb_client+i*(400/nb_client),100,50-nb_client*1.5);
      m_le_setUp[i][1]->setValidator(limite);
    }

    m_bouton_retourMenu = new  QPushButton("Retourner Au Menu",this);
    m_bouton_retourMenu->setGeometry(50,530,350,50);

    m_bouton_valider = new QPushButton("Valider",this);
    m_bouton_valider->setGeometry(400,530,350,50);

    connect(m_bouton_valider,SIGNAL(clicked()),this,SLOT(push_valider()));
    connect(m_bouton_retourMenu,SIGNAL(clicked()),this,SLOT(push_retourMenu()));
}

void Fenetre_newSimulation::push_valider(){
    float setup[nb_client][2];

    for(int i=0;i<nb_client;i++){
        //Taux d'emision du client i
        if(m_le_setUp[i][0]->text().isEmpty())
            setup[i][0] = 0.0; //Valeur par defaut
        else
            setup[i][0] = m_le_setUp[i][0]->text().toFloat();

        //Taux de génération des jetons
        if(m_le_setUp[i][1]->text().isEmpty())
            setup[i][1] = 1.0; //Valeur par defaut
        else
            setup[i][1] = m_le_setUp[i][1]->text().toFloat();
    }

    this->close();
    Fenetre_simulation* simulation = new Fenetre_simulation(nb_client,setup);
    QGraphicsView* vue = new QGraphicsView(simulation);
    vue->setMouseTracking(true);
    vue->setFixedSize(LONGUEUR+5,LARGEUR+5);
    vue->setSceneRect(vue->sceneRect());
    vue->show();
    //TEST)
    connect(simulation,SIGNAL(sceneRectChanged(QRectF)),vue,SLOT(close()));
}

void Fenetre_newSimulation::push_retourMenu(){
  this->close();
  Fenetre_menu* menu = new Fenetre_menu();
  menu->show();
}
