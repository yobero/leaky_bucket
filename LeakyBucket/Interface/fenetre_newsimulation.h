#ifndef FENETRE_NEWSIMULATION_H
#define FENETRE_NEWSIMULATION_H

#pragma once

#include <QGraphicsView>
#include <QDoubleValidator>

#include "fenetre_menu.h"
#include "fenetre_simulation.h"

class Fenetre_newSimulation : public QWidget
{
Q_OBJECT

public:
    Fenetre_newSimulation(const int nb_client);

public slots:
    void push_valider();
    void push_retourMenu();

private:
    QPushButton *m_bouton_valider;    	/* Permet de valider les contrats et de passer à la simulation */
    QPushButton *m_bouton_retourMenu;	/* Permet de retourner au menu de sélection */
    QLabel *m_texte; 			/* afficher ce que l’utilisateur doit faire au dessus de la page */
    QLabel *m_config_client[2];  	/* Affiche les colonnes*/
    QLabel *m_numClient[10]; 		/* Pour afficher le numéro du client au dessus de chaque QLineEdit */
    QLineEdit *m_le_setUp[10][2]; 	/* Case permettant à l’utilisateur de fixer le contrat pour chaque client (c’est un tableau car la 						*  taille sera initialisée en fonction du nombre de clients choisis par l’utilisateur
    					*  10 ==> le nombre de client et 2 ==> les deux parametres à entrer 
					*/
    QDoubleValidator *limite; 		/*Autorise seulement les chiffres dans un QLinEdit et fixe l'intervalle */

    int nb_client;			/* Le nombre de client simulés */
};

#endif // FENETRE_NEWSIMULATION_H
