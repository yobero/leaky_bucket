QT += widgets
QMAKE_CXXFLAGS += -std=c++0x

SOURCES += \
    main.cpp \
    Interface/fenetre_simulation.cpp \
    Interface/fenetre_loading.cpp \
    Interface/fenetre_newsimulation.cpp \
    Interface/fenetre_menu.cpp \
    sources/client.cpp \
    sources/token.cpp \
    sources/bufferCell.cpp \
    sources/cell.cpp \
    sources/save.cpp \
    sources/stats.cpp \
    sources/test.cpp

HEADERS += \
    Interface/fenetre_simulation.h \
    Interface/fenetre_loading.h \
    Interface/fenetre_newsimulation.h \
    Interface/fenetre_menu.h \
    headers/client.h \
    headers/token.h \
    headers/bufferCell.h \
    headers/cell.h \
    headers/save.h \
    headers/stats.h \
    headers/test.h
