#include "../headers/cell.h"
#include "../headers/bufferCell.h"
#include "../headers/token.h"
#include "../headers/client.h"
#include "../headers/save.h"
#include "../headers/test.h"
#include "../headers/stats.h"


// Main avec les tests
int main()
{
  char answer = 'N';
  printf("Voulez-vous lancer les tests de Token ? (O/N) \n");
  scanf("%s", &answer);
  if(answer == 'O')
  {
    //test de la classe Token
    test_void_constructor();
    test_constructor(5);
    test_get_length();
    test_get_bufferToken();
    test_move_to_left();
    test_add_token();
    test_to_String();
  }
  printf("Voulez-vous lancer les tests de Cell ? (O/N) \n");
  scanf("%s", &answer);
  if(answer == 'O')
  {
    //test de la classe Cell
    test_void_constructor_cell();
    test_constructor(0,3);
    test_get_priority();
    test_get_type();
    test_set_priority(1);
    test_set_type(2);
  }
  printf("Voulez-vous lancer les tests de BufferCell ? (O/N) \n");
  scanf("%s", &answer);
  if(answer == 'O')
  {
    //test de la classe bufferCell
    test_void_constructor_BufferCell();
    test_constructor_BufferCell(5);
    test_to_string_BufferCell();
    test_add_Cell(Cell(0,2));
    test_take_Cell();
    test_move_to_left_bufferCell();
  }
  printf("Voulez-vous lancer les tests de client ? (O/N) \n");
  scanf("%s", &answer);
  if(answer == 'O')
  {
    //test de la classe Client
    test_constructor_void_client();
    test_constructor_client (1, 10.5, 9.5);
    test_get_on();
    test_set_on(true);
    test_get_idClient();
    test_set_idClient(2);
    test_get_tokens_rate();
    test_set_tokens_rate(50.2);
    test_get_transmit_rate();
    test_set_transmit_rate(42);
    test_get_nb_cell_transmit();
    test_set_nb_cell_transmit (5000);
    test_get_nb_lost_cell();
    test_set_nb_lost_cell(80);
  }

  //test du programme principal
  cout << "La suite est un test du fonctionnement de la simulation où les paramètres sont à rentrer dans le code lui-même" << endl;
  int timer = 5;
  int nb_clients_actif;

  //création des clients
  printf("Avec combien de clients voulez-vous commencer la simulation ? \n");
  scanf("%d", &nb_clients_actif);
  if(nb_clients_actif == 0)
  {
    return 0;
  }
  cout << "Vous avez choisi d'effectuer une simulation avec " << nb_clients_actif << " clients. \n" << endl;
  Client* all_clients = new Client[nb_clients_actif];

  //préremplissage
  cout << "l'id du client, ses taux de génération de cellules et de jetons par tour de boucle sont choisi de manière arbitraire" << endl;
  for(int i = 0; i < nb_clients_actif; i++)
  {
    all_clients[i] = Client(i+1,i+11,1);
    cout << "Client " << i << " :" << endl;
    cout << "ID : " << i+1 << endl;
    cout << "taux d'émission des cellules : " << i+2 << endl;
    cout << "taux d'émission des jetons : " << i+3 << endl;
  }
  cout << "Pour simuler un timer on va parcourir "<< timer << " fois une boucle \n" << endl;
  //timer pour remplacer les secondes
  while(timer > 0)
  {
    //pour chaque client actifs
    for(int l = 0; l < nb_clients_actif; l++)
    {
      cout << "Traitement du client " << l << endl;
      //ajoute les jetons suivant le taux
      for(int k = 0; k < all_clients[l].get_tokens_rate(); k++)
      {
        all_clients[l].get_tokens().add_Token();
      }
      cout << "on ajoute ses jetons aux buffers de jetons (taille 10). Il doit en avoir au moins " << all_clients[l].get_tokens_rate() << endl;
      cout << "Jetons : " << all_clients[l].get_tokens().to_String() << "\n" << endl;

      //ajoute les cellules suivant le taux
      cout << "on ajoute ensuite le nombre de cellule qu'il souhaite émettre pour ce tour de boucle" << endl;
      all_clients[l].set_nb_cell_transmit(all_clients[l].get_nb_cell_transmit()+all_clients[l].get_transmit_rate());
      cout << "Le client a émit " << all_clients[l].get_nb_cell_transmit() << " cellules jusqu'à maintenant" << endl;
      cout << "il peut émettre jusqu'à " << all_clients[l].get_transmit_rate() << " cellules" << endl;
      for(int j = 0; j < all_clients[l].get_transmit_rate(); j++)
      {
        cout << "actuellement la file de cellule est la suivante \n" << all_clients[l].get_cells().to_String() << endl;
        if (all_clients[l].get_cells().add_Cell(Cell(l+1,(l%3)+1))==true)
        {
          all_clients[l].set_nb_lost_cell(all_clients[l].get_nb_lost_cell()+1);
          cout << "Une cellule a été perdue par cause de remplacement. Actuellement il y a " << all_clients[l].get_nb_lost_cell() << "cellules perdues" << endl;
        }
        cout << "après l'ajout la file de cellules est la suivante \n" << all_clients[l].get_cells().to_String() << endl;
      }

      //envoie tant que des jetons et des cellules
      while(all_clients[l].get_tokens().get_bufferToken()[0] != 0 && all_clients[l].get_cells().cells_void() == false)
      {
        all_clients[l].get_tokens().move_to_left();
        all_clients[l].get_cells().move_to_left();
        cout << "le client " << all_clients[l].get_idClient() << " a envoyé une cellule sur le réseau" << endl;
        cout << "après l'envoie la file de cellules est la suivante \n" << all_clients[l].get_cells().to_String() << endl;
        cout << "après l'envoie la file de jetons est la suivante \n" << all_clients[l].get_tokens().to_String() << endl;
      }
        cout << "cellules perdues / transmises du client " << all_clients[l].get_idClient() << " " << lost_cells_rate(all_clients[l]) << endl;
    }
    timer--;
    cout << "cellules perdues total " << total_lost_cells(all_clients, nb_clients_actif) << endl;
    cout << "cellules envoyées total " << total_sent_cells(all_clients, nb_clients_actif) << endl;
    cout << "cellules perdues en moyenne au total " << lost_cell_average(all_clients, nb_clients_actif) << endl;
  }
  //pas nécessaire
  //ecriture(all_clients, nb_clients_actif);
}
