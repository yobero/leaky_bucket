#include "../headers/stats.h"

/**
* Parcourt le tableau de clients afin de déterminer le nombre total de cellules émises par ces clients depuis le début de la simulation.
* Entrées : Client *clients
* Sortie : nombre total de cellules envoyées
*/
int total_sent_cells(Client *clients, int length)
{
  int total=0;
  for(int i = 0; i < length ; i++)
  {
    total += clients[i].get_nb_cell_transmit();
  }
  return total;
}

/**
* Parcourt le tableau de clients afin de déterminer le nombre total de cellules perdues par ces clients depuis le début de la simulation.
* Entrées : Client *clients
* Sortie : nombre total de cellules perdues
*/
int total_lost_cells(Client *clients, int length)
{
  int total=0;
  for(int i = 0; i < length ; i++)
  {
    total += clients[i].get_nb_lost_cell();
  }
  return total;
}

/**
* Calcul de la moyenne de cellules perdues par l'ensemble des clients de la simulation
* Entrées : Client *clients
* Sortie : nombre de cellules perdues en moyenne par l'ensemble des clients
*/
float lost_cell_average(Client *clients, int length)
{
  int cells_lost = total_lost_cells(clients, length);
  int cells_transmit = total_sent_cells(clients, length);
  if(cells_transmit == 0)
	return 0;
  return ((float)cells_lost/(float)cells_transmit) *100;
}

/**
* Calcul le taux de perte d'un client de la simulation
* Entrées : Client *clients
* Sortie : rapport des cellules perdues sur les cellules émises
*/
float lost_cells_rate(Client c)
{
  if(c.get_nb_cell_transmit() == 0)
	return 0;
  return ((float)c.get_nb_lost_cell()/(float)c.get_nb_cell_transmit()) *100;
}

/**
* Calcul le taux de perte de l'ensemble des clients de la simulation.
* Entrées : Client *clients
* Sortie : float total_rate
*/
float total_lost_cells_rate(Client* clients, int length)
{
  float total_rate = 0;
  for(int i = 0; i < length ; i++)
  {
    total_rate += lost_cells_rate(clients[i]);
  }
  return total_rate/length;
}
