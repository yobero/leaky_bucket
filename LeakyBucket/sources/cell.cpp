#include "../headers/cell.h"
/**
* Constructeur vide de cell
* Entrées : aucune
* Sortie : aucune
*/
Cell::Cell(){}

/**
* Constructeur vide de cell
* Entrées : int priority, int type
* Sortie : aucune
*/
Cell::Cell(int priority, int type)
{
    this->priority = priority;
    this->type = type;
}

/**
* Destructeur de cell
* Entrées : aucune
* Sortie : aucune
*/
Cell::~Cell(){}

/**
* Accesseur de priority
* Entrées : aucune
* Sortie : retourne priority
*/
int Cell::getPriority()
{
    return this->priority;
}

/**
* Accesseur de type
* Entrées : aucune
* Sortie : retourne type
*/
int Cell::getType()
{
    return this->type;
}

/**
* Mutateur de priority
* Entrées : int priority
* Sortie : aucune
*/
void Cell::setPriority(int priority)
{
    this->priority = priority;
}

/**
* Mutateur de type
* Entrées : int priority
* Sortie : aucune
*/
void Cell::setType(int type)
{
    this->type = type;
}
