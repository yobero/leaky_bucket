#include "cell.h"
#include <string>

#ifndef BUFFERCELL_H_INCLUDED
#define BUFFERCELL_H_INCLUDED

using std::string;

class BufferCell
{
  private:
    Cell* cells;
    int length;
  public:
    BufferCell();
    BufferCell(int aLength);
    ~BufferCell();
    bool add_Cell(Cell aCell);
    Cell* get_cells();
    void take_Cell();
    void move_to_left();
    string to_String();
    bool cells_void();
};

#endif
