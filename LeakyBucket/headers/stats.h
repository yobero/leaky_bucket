#include "../headers/client.h"

int total_sent_cells(Client *clients, int length);
int total_lost_cells(Client *clients, int length);
float lost_cell_average(Client *clients, int length);
float lost_cells_rate(Client c);
float total_lost_cells_rate(Client *clients, int length);
