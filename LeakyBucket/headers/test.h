#include "cell.h"
#include "bufferCell.h"
#include "token.h"
#include "client.h"
#include "save.h"
#include <iostream>

using namespace std;

//test de la classe Token
void test_void_constructor();
void test_constructor(int length);
void test_get_length();
void test_get_bufferToken();
void test_move_to_left();
void test_add_token();
void test_to_String();

  //test de la classe Cell
void test_void_constructor_cell();
void test_constructor(int priority, int type);
void test_get_priority();
void test_get_type();
void test_set_priority(int priority);
void test_set_type(int type);
void test_void_constructor_BufferCell();

//test de la classe bufferCell
void test_constructor_BufferCell(int aLength);
void test_to_string_BufferCell();
void test_add_Cell(Cell aCell);
void test_take_Cell();
void test_move_to_left_bufferCell();

//test de la classe Client
void test_constructor_void_client();
void test_constructor_client (int id, float transmitRate, float geneTokensRate);
void test_get_on();
void test_set_on(bool aBool);
void test_get_idClient();
void test_set_idClient(int idClient);
void test_get_tokens_rate();
void test_set_tokens_rate(float tokensRate);
void test_get_transmit_rate();
void test_set_transmit_rate(float transmitRate);
void test_get_nb_cell_transmit();
void test_set_nb_cell_transmit (int nbCellTransmit);
void test_get_nb_lost_cell();
void test_set_nb_lost_cell(int nbCellLost);
