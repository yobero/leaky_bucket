#include <QApplication>
#include <QTextCodec>

#include "Interface/fenetre_menu.h"

int main(int argc, char *argv[])

{
    // UTF-8 Encoding
    QTextCodec::setCodecForCStrings(QTextCodec::codecForName("UTF-8"));
    QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));
    QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));

    QApplication app(argc, argv);

   Fenetre_menu menu;
    menu.show();

   return app.exec();

}
